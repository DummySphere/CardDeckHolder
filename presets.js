
addPreset({
	name: "Équipement de départ",
	cardWidth: 41,
	cardHeight: 63,
	cardThickness: 0.4,
	cardCount: 20,
	cardTopMargin: 20,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [],
	texts: [
		{ face: 5, text: "ÉQUIPEMENT", x: 0, y: -12.75, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 5, text: "DE DÉPART", x: 0, y: -7.25, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "ÉQUIPEMENT", x: 0, y: -12.75, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "DE DÉPART", x: 0, y: -7.25, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Trésor Commun",
	cardWidth: 41,
	cardHeight: 63,
	cardThickness: 0.4,
	cardCount: 50,
	cardTopMargin: 20,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [
		{ face: 5, href: "images/TreasureIcon_Common.png", x: 0, y: 5, width: 13, height: 13 },
		{ face: 12, href: "images/TreasureIcon_Common.png", x: 0, y: 5, width: 13, height: 13 },
	],
	texts: [
		{ face: 5, text: "TRÉSOR", x: 0, y: -11.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 5, text: "COMMUN", x: 0, y: -6, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "TRÉSOR", x: 0, y: -11.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "COMMUN", x: 0, y: -6, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Trésor Rare",
	cardWidth: 41,
	cardHeight: 63,
	cardThickness: 0.4,
	cardCount: 50,
	cardTopMargin: 20,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [
		{ face: 5, href: "images/TreasureIcon_Rare.png", x: 0, y: 5, width: 13, height: 13 },
		{ face: 12, href: "images/TreasureIcon_Rare.png", x: 0, y: 5, width: 13, height: 13 },
	],
	texts: [
		{ face: 5, text: "TRÉSOR", x: 0, y: -11.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 5, text: "RARE", x: 0, y: -6, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "TRÉSOR", x: 0, y: -11.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "RARE", x: 0, y: -6, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Trésor Épic",
	cardWidth: 41,
	cardHeight: 63,
	cardThickness: 0.4,
	cardCount: 45,
	cardTopMargin: 20,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [
		{ face: 5, href: "images/TreasureIcon_Epic.png", x: 0, y: 5, width: 13, height: 13 },
		{ face: 12, href: "images/TreasureIcon_Epic.png", x: 0, y: 5, width: 13, height: 13 },
	],
	texts: [
		{ face: 5, text: "TRÉSOR", x: 0, y: -11.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 5, text: "ÉPIQUE", x: 0, y: -6, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "TRÉSOR", x: 0, y: -11.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "ÉPIQUE", x: 0, y: -6, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Trésor Légendaire",
	cardWidth: 41,
	cardHeight: 63,
	cardThickness: 0.4,
	cardCount: 38,
	cardTopMargin: 20,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [
		{ face: 5, href: "images/TreasureIcon_Legendary.png", x: 0, y: 5, width: 13, height: 13 },
		{ face: 12, href: "images/TreasureIcon_Legendary.png", x: 0, y: 5, width: 13, height: 13 },
	],
	texts: [
		{ face: 5, text: "TRÉSOR", x: 0, y: -11.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 5, text: "LÉGENDAIRE", x: 0, y: -6, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "TRÉSOR", x: 0, y: -11.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "LÉGENDAIRE", x: 0, y: -6, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Objet de Bande Niveaux 1-2",
	cardWidth: 41,
	cardHeight: 63,
	cardThickness: 0.4,
	cardCount: 24,
	cardTopMargin: 20,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [],
	texts: [
		{ face: 5, text: "OBJET DE BANDE", x: 0, y: -10, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 5, text: "NIVEAUX 1-2", x: 0, y: -4.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "OBJET DE BANDE", x: 0, y: -10, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "NIVEAUX 1-2", x: 0, y: -4.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Objet de Bande Niveaux 3-4",
	cardWidth: 41,
	cardHeight: 63,
	cardThickness: 0.4,
	cardCount: 24,
	cardTopMargin: 20,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [],
	texts: [
		{ face: 5, text: "OBJET DE BANDE", x: 0, y: -10, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 5, text: "NIVEAUX 3-4", x: 0, y: -4.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "OBJET DE BANDE", x: 0, y: -10, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "NIVEAUX 3-4", x: 0, y: -4.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Objet de Bande Niveau 5",
	cardWidth: 41,
	cardHeight: 63,
	cardThickness: 0.4,
	cardCount: 24,
	cardTopMargin: 20,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [],
	texts: [
		{ face: 5, text: "OBJET DE BANDE", x: 0, y: -10, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 5, text: "NIVEAU 5", x: 0, y: -4.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "OBJET DE BANDE", x: 0, y: -10, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "NIVEAU 5", x: 0, y: -4.5, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Porte",
	cardWidth: 63,
	cardHeight: 88.5,
	cardThickness: 0.4,
	cardCount: 30,
	cardTopMargin: 20,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [
		{ face: 5, href: "images/DoorIcon.png", x: 0, y: -10, width: 40, height: 40 },
		{ face: 12, href: "images/DoorIcon.png", x: 0, y: -10, width: 40, height: 40 },
	],
	texts: [
		{ face: 5, text: "PORTE", x: 0, y: -10, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "PORTE", x: 0, y: -10, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Bande Niveaux 1-2",
	cardWidth: 63,
	cardHeight: 88.5,
	cardThickness: 0.4,
	cardCount: 14,
	cardTopMargin: 20,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [
		{ face: 5, href: "images/EnemyIcon_Mob.png", x: 0, y: -13, width: 42, height: 42 },
		{ face: 12, href: "images/EnemyIcon_Mob.png", x: 0, y: -13, width: 42, height: 42 },
	],
	texts: [
		{ face: 5, text: "NIVEAUX 1-2", x: 0, y: 18, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "NIVEAUX 1-2", x: 0, y: 18, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Bande Niveaux 3-4",
	cardWidth: 63,
	cardHeight: 88.5,
	cardThickness: 0.4,
	cardCount: 14,
	cardTopMargin: 20,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [
		{ face: 5, href: "images/EnemyIcon_Mob.png", x: 0, y: -13, width: 42, height: 42 },
		{ face: 12, href: "images/EnemyIcon_Mob.png", x: 0, y: -13, width: 42, height: 42 },
	],
	texts: [
		{ face: 5, text: "NIVEAUX 3-4", x: 0, y: 18, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "NIVEAUX 3-4", x: 0, y: 18, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Bande Niveau 5",
	cardWidth: 63,
	cardHeight: 88.5,
	cardThickness: 0.4,
	cardCount: 14,
	cardTopMargin: 20,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [
		{ face: 5, href: "images/EnemyIcon_Mob.png", x: 0, y: -13, width: 42, height: 42 },
		{ face: 12, href: "images/EnemyIcon_Mob.png", x: 0, y: -13, width: 42, height: 42 },
	],
	texts: [
		{ face: 5, text: "NIVEAU 5", x: 0, y: 18, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "NIVEAU 5", x: 0, y: 18, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Monstre Errant Niveaux 1-2",
	cardWidth: 80,
	cardHeight: 120,
	cardThickness: 0.4,
	cardCount: 14,
	cardTopMargin: 30,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [
		{ face: 5, href: "images/EnemyIcon_RoamingMonster.png", x: 0, y: -18, width: 42, height: 42 },
		{ face: 12, href: "images/EnemyIcon_RoamingMonster.png", x: 0, y: -18, width: 42, height: 42 },
	],
	texts: [
		{ face: 5, text: "NIVEAUX 1-2", x: 0, y: 18, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "NIVEAUX 1-2", x: 0, y: 18, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Monstre Errant Niveaux 3-4",
	cardWidth: 80,
	cardHeight: 120,
	cardThickness: 0.4,
	cardCount: 14,
	cardTopMargin: 30,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [
		{ face: 5, href: "images/EnemyIcon_RoamingMonster.png", x: 0, y: -18, width: 42, height: 42 },
		{ face: 12, href: "images/EnemyIcon_RoamingMonster.png", x: 0, y: -18, width: 42, height: 42 },
	],
	texts: [
		{ face: 5, text: "NIVEAUX 3-4", x: 0, y: 18, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "NIVEAUX 3-4", x: 0, y: 18, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Monstre Errant Niveau 5",
	cardWidth: 80,
	cardHeight: 120,
	cardThickness: 0.4,
	cardCount: 14,
	cardTopMargin: 30,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [
		{ face: 5, href: "images/EnemyIcon_RoamingMonster.png", x: 0, y: -18, width: 42, height: 42 },
		{ face: 12, href: "images/EnemyIcon_RoamingMonster.png", x: 0, y: -18, width: 42, height: 42 },
	],
	texts: [
		{ face: 5, text: "NIVEAU 5", x: 0, y: 18, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
		{ face: 12, text: "NIVEAU 5", x: 0, y: 18, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 22 },
	],
});

addPreset({
	name: "Compétence",
	cardWidth: 41,
	cardHeight: 63,
	cardThickness: 0.4,
	cardCount: 13,
	cardTopMargin: 20,
	innerTopHeight: 25,
	innerLeftWidth: 20,
	widthMargin: 1,
	thicknessMargin: 0,
	bendMargin: 1,
	imageMargin: 5,
	images: [
		{ face: 5, href: "images/CampaignIcon.png", x: 0, y: -19, width: 10, height: 10 },
		{ face: 12, href: "images/CampaignIcon.png", x: 0, y: -19, width: 10, height: 10 },
	],
	texts: [
		{ face: 5, text: "COMPÉTENCE", x: 0, y: 17, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 11 },
		{ face: 12, text: "COMPÉTENCE", x: 0, y: 17, fontFamily: "DwarvenAxeBBW00-Regular", fontSize: 11 },
	],
});
