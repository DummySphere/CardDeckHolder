
function mm(value) { return +value() + "mm"; }

function has_font_family(fontName) {
    // creating our in-memory Canvas element where the magic happens
    var canvas = document.createElement("canvas");
    var context = canvas.getContext("2d");
     
    // the text whose final pixel size I want to measure
    var text = "abcdefghijklmnopqrstuvwxyz0123456789";
     
    // specifying the baseline font
    context.font = "72px monospace";
     
    // checking the size of the baseline text
    var baselineSize = context.measureText(text).width;
     
    // specifying the font whose existence we want to check
    context.font = "72px '" + fontName + "', monospace";
     
    // checking the size of the font we want to check
    var newSize = context.measureText(text).width;
     
    // removing the Canvas element we created
    canvas = null;

	return newSize !== baselineSize;
}

var Image = function(face, href, x, y, width, height) {
	this.face = ko.observable(face);
	this.href = ko.observable(href);
	this.x = ko.observable(x);
	this.y = ko.observable(y);
	this.width = ko.observable(width);
	this.height = ko.observable(height);
};

var Text = function(face, text, x, y, fontFamily, fontSize) {
	this.face = ko.observable(face);
	this.text = ko.observable(text);
	this.x = ko.observable(x);
	this.y = ko.observable(y);
	this.fontFamily = ko.observable(fontFamily);
	this.fontSize = ko.observable(fontSize);
};

var Line = function(x1, y1, x2, y2) {
	this.x1 = x1;
	this.y1 = y1;
	this.x2 = x2;
	this.y2 = y2;
};

var Face = function(x, y, width, height, classes, angle) {
	this.x = x;
	this.y = y;
	this.width = width;
	this.height = height;
	this.angle = angle || ko.observable(0);
	this.classes = classes || "";
	this.center_x = ko.pureComputed(function() { return +x() + width() / 2; }, this);
	this.center_y = ko.pureComputed(function() { return +y() + height() / 2; }, this);
	this.images = [];
	this.texts = [];
};

var addPreset;

var viewModel = function() {
	var self = this;

	// parameters

	this.cardWidth = ko.observable(41);
	this.cardHeight = ko.observable(63);
	this.cardThickness = ko.observable(0.4);
	this.cardCount = ko.observable(50);
	this.deckThickness = ko.pureComputed(function() { return +this.cardThickness() * +this.cardCount(); }, this);

	this.cardTopMargin = ko.observable(20);
	this.holderHeight = ko.pureComputed(function() { return +this.cardHeight() - +this.cardTopMargin(); }, this);

	this.innerTopHeight = ko.observable(25);
	this.innerLeftWidth = ko.observable(20);

	this.widthMargin = ko.observable(1);
	this.thicknessMargin = ko.observable(0);
	this.bendMargin = ko.observable(1);

	this.width = ko.pureComputed(function() { return +this.cardWidth() + +this.widthMargin(); }, this);
	this.height = ko.pureComputed(function() { return +this.holderHeight(); }, this);
	this.thickness = ko.pureComputed(function() { return +this.deckThickness() + +this.thicknessMargin(); }, this);

	this.imageMargin = ko.observable(5);
	
	this.has_font = ko.observable(false);
	// this.has_font = ko.observable(has_font_family("DwarvenAxeBBW00-Regular")); // error on mobile Chrome

	// coordinates
	
	var y_0 = ko.pureComputed(function() { return +this.imageMargin(); }, this);
	var y_1 = ko.pureComputed(function() { return +y_0() + +this.innerTopHeight(); }, this);
	var y_2 = ko.pureComputed(function() { return +y_1() + this.height(); }, this);
	var y_3 = ko.pureComputed(function() { return +y_2() + this.thickness() + +this.bendMargin(); }, this);
	var y_4 = ko.pureComputed(function() { return +y_3() + this.height() + +this.bendMargin(); }, this);
	this.imageHeight = ko.pureComputed(function() { return +y_4() + +this.innerTopHeight() + +this.imageMargin(); }, this);

	var x_0 = ko.pureComputed(function() { return +this.imageMargin(); }, this);
	var x_1 = ko.pureComputed(function() { return +x_0() + +this.innerLeftWidth(); }, this);
	var x_2 = ko.pureComputed(function() { return +x_1() + this.thickness(); }, this);
	var x_3 = ko.pureComputed(function() { return +x_2() + this.width(); }, this);
	var x_4 = ko.pureComputed(function() { return +x_3() + this.thickness(); }, this);
	var x_5 = ko.pureComputed(function() { return +x_4() + this.width(); }, this);
	this.imageWidth = ko.pureComputed(function() { return +x_5() + this.thickness() + +this.imageMargin(); }, this);

	var reversed_angle = ko.observable(180);

	// faces
	
	var inner_top_left = new Face(x_1, y_0, this.thickness, this.innerTopHeight, "inside insideTop");
	var inner_top_front = new Face(x_2, y_0, this.width, this.innerTopHeight, "inside insideTop");
	var inner_top_right = new Face(x_3, y_0, this.thickness, this.innerTopHeight, "inside insideTop");

	var inner_left = new Face(x_0, y_1, this.innerLeftWidth, this.height, "inside insideLeft");
	var left = new Face(x_1, y_1, this.thickness, this.height, "outside");
	var front = new Face(x_2, y_1, this.width, this.height, "outside");
	var right = new Face(x_3, y_1, this.thickness, this.height, "outside");
	var inside_back = new Face(x_4, y_1, this.width, this.height, "inside");
	var inside_left = new Face(x_5, y_1, this.thickness, this.height, "inside");

	var bottom_left = new Face(x_1, y_2, this.thickness, this.width, "inside");
	var bottom = new Face(x_2, y_2, this.width, ko.pureComputed(function() { return +this.thickness() + +this.bendMargin(); }, this), "outside bendMargin", reversed_angle);
	var bottom_right = new Face(x_3, y_2, this.thickness, this.width, "inside");

	var back = new Face(x_2, y_3, this.width, ko.pureComputed(function() { return +this.height() + +this.bendMargin(); }, this), "outside bendMargin", reversed_angle);

	var inner_top_back = new Face(x_2, y_4, this.width, this.innerTopHeight, "inside insideTop", reversed_angle);

	this.faces = ko.observableArray([
		inner_top_left,
		inner_top_front,
		inner_top_right,
		
		inner_left,
		left,
		front,
		right,
		inside_back,
		inside_left,
		
		bottom_left,
		bottom,
		bottom_right,
		
		back,
		
		inner_top_back,
	]);

	// lines
	
	this.lines = ko.observableArray([
		new Line(x_1, y_1, x_4, y_1),
		new Line(x_1, y_2, x_4, y_2),
		new Line(x_2, y_3, x_3, y_3),
		new Line(x_2, y_4, x_3, y_4),
		new Line(x_1, y_1, x_1, y_2),
		new Line(x_2, y_1, x_2, y_2),
		new Line(x_3, y_1, x_3, y_2),
		new Line(x_4, y_1, x_4, y_2),
		new Line(x_5, y_1, x_5, y_2),
	]);

	// images
	
	this.images = ko.observableArray([]);
	this.addImage = function() {
		self.images.push(new Image(0, "images/placeholder.png", 0, 0, 10, 10));
	}
	this.removeImage = function(image) {
		self.images.remove(image);
	}

	// texts

	this.texts = ko.observableArray([]);
	this.addText = function() {
		self.texts.push(new Text(0, "Lorem Ipsum", 0, 0));
	}
	this.removeText = function(text) {
		self.texts.remove(text);
	}
	
	// presets
	
	this.presets = ko.observableArray([]);
	this.selectedPreset = ko.observable();
	var settingPreset = false;
	
	addPreset = function(preset) {
		self.presets.push(preset);
	};
	
	var params = [
		'cardWidth',
		'cardHeight',
		'cardThickness',
		'cardCount',
		'cardTopMargin',
		'innerTopHeight',
		'innerLeftWidth',
		'widthMargin',
		'thicknessMargin',
		'bendMargin',
		'imageMargin',
		'images',
		'texts',
	];
	
	params.forEach(function(param) {
		self[param].subscribe(function(newValue) {
			if(settingPreset)
				return;
			self.selectedPreset(null);
		});
	});
	
	this.selectedPreset.subscribe(function(preset) {
		if(preset == null)
			return;
		settingPreset = true;
		
		params.forEach(function(param) {
			var observable = self[param];
			var paramValue = preset[param];
			if(paramValue != null)
			{
				if(param == "images")
				{
					observable.removeAll();
					
					paramValue.forEach(function(image) {
						observable.push(new Image(image.face || 0, image.href || "images/placeholder.png", image.x || 0, image.y || 0, image.width || 10, image.height || 10));
					});
				}
				else if(param == "texts")
				{
					observable.removeAll();
					
					paramValue.forEach(function(text) {
						observable.push(new Text(text.face || 0, text.text || "Lorem Ipsum", text.x || 0, text.y || 0, text.fontFamily, text.fontSize));
					});
				}
				else
					self[param](paramValue);
			}
		});
		
		settingPreset = false;
	});
};

ko.applyBindings(new viewModel());
